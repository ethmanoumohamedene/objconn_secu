import asyncio
from bleak import BleakClient, BleakScanner



import asyncio
from asyncio import Queue

async def notify(client, char_uuid):
    # Fonction de rappel pour gérer les notifications
    def notification_handler(sender, data):
        print(data)
        
        


    # Activer les notifications pour la caractéristique spécifiée
    await client.start_notify(char_uuid, notification_handler)

    try:
        # Attendez un moment pour que les notifications soient reçues
        await asyncio.sleep(10)  # Changez cette valeur selon vos besoins

    except Exception as e:
        print(f"Erreur lors de la lecture de la caractéristique: {e}")

    finally:
        # Arrêter les notifications avant de quitter la fonction
        await client.stop_notify(char_uuid)
        











async def read_gatt_characteristic(client , char_specifier):
        data = await client.read_gatt_char(char_specifier)
        print("Données lues:", data)





async def write_gatt_characteristic( client , char_specifier):
        data_to_write = b"\x04\x00" 
        await client.write_gatt_char(char_specifier, data_to_write)
        print("Données écrites avec succès.")




async def get_device_by_address(address):
    devices = await BleakScanner.discover()
    for device in devices:
        if device.address == address:
            return device

async def print_device_details(device , char_specifier , char_uuid):
    print(device.details)
    try:
        async with BleakClient(device.address) as client:
            if client.is_connected:
                print("Connected")
               
                await write_gatt_characteristic(client , char_uuid)
                await notify(client , char_uuid )
                await read_gatt_characteristic(client , char_specifier)
                

    except Exception as e:
        print(f"Error: {e}")

async def main():
    address = "D1:6A:16:01:A2:EC"
    char_specifier = "1b0d1303-a720-f7e9-46b6-31b601c4fca1"
    char_specifier1 = "1b0d1302-a720-f7e9-46b6-31b601c4fca1"
    device = await get_device_by_address(address)
    if device:
        await print_device_details(device , char_specifier , char_specifier1)
        
    else:
        print("Device not found.")

asyncio.run(main())
