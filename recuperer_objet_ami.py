import asyncio
import hashlib
import time
from bleak import BleakClient, BleakError

async def my_notification_callback(characteristic , data):
    # Construire le nom du fichier
    filename = "donnees_recues.bin"
    
    # Écrire les données d'octets dans le fichier
    with open(filename, "ab") as file:  # Utilisez "ab" pour ajouter les données à la fin du fichier
        file.write(data)  # Ajouter un saut de ligne après chaque enregistrement
    
    print(f"Received notification and wrote hex data to {filename}")




async def connect_and_operate(address):
    try:
        async with BleakClient(address, timeout=10000.0) as client:
            # Connexion réussie, opérations à effectuer sur le périphérique
            services = await client.get_services()

            for service in services:
                print(f"Service UUID: {service.uuid}")
                for characteristic in service.characteristics:
                    if characteristic.uuid == "1b0d1303-a720-f7e9-46b6-31b601c4fca1":
                        await client.start_notify("1b0d1303-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                        print(f"Characteristic UUID: {characteristic.uuid}")
                        data = await client.read_gatt_char(characteristic)
                        print("Read data:", data.hex())
                        await client.start_notify("1b0d1302-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                        await client.write_gatt_char("1b0d1302-a720-f7e9-46b6-31b601c4fca1", data)
                        # Construct the data payload
                        name = b"T10\x00\x00\x00\x00\x00"
                        chunkId = b"\x00\x00\x00\x00"
                        numChunk = b"\x00\x00\x27\x0F"
                        Filerequest = name + chunkId + numChunk
                        await client.start_notify("1b0d1304-a720-f7e9-46b6-31b601c4fca1", callback=my_notification_callback)
                        await client.write_gatt_char("1b0d1304-a720-f7e9-46b6-31b601c4fca1", Filerequest)

            
            # Attendez un certain temps pour recevoir les notifications
            await asyncio.sleep(10)
                        
    except BleakError as e:
        print(f"Error occurred while connecting to {address}: {e}")

async def main():
    try:
        device_address = "CF:64:52:65:9F:67"
        await connect_and_operate(device_address)
    except BleakError as e:
        print(f"Error occurred: {e}")

asyncio.run(main())
